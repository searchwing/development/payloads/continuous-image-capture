FROM ros:iron

ARG USERNAME=searchwing
ARG USER_UID=1000
ARG USER_GID=$USER_UID

WORKDIR /home/$USERNAME
ENV HOME=/home/$USERNAME

# Paths for Gazebo, Ardupilot_Gazebo
ENV GZ_SIM_SYSTEM_PLUGIN_PATH="$HOME/ardupilot_gazebo/build:$HOME/install/lib:$GZ_SIM_SYSTEM_PLUGIN_PATH"
ENV GZ_SIM_RESOURCE_PATH="$HOME/ws/src/simulation/simulation/models:$HOME/ws/src/simulation/simulation/worlds:$HOME/ardupilot_gazebo/models:$HOME/ardupilot_gazebo/worlds:$GZ_SIM_RESOURCE_PATH"
ENV GZ_IP=127.0.0.1
ENV GZ_CONFIG_PATH="/usr/share/gz"
ENV PATH="$PATH:$HOME/.local/bin::$HOME/ardupilot/Tools/autotest:/usr/lib/ccache:"
ENV GZ_VERSION=harmonic

RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && apt-get update \
    && apt-get install -y sudo \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && chmod 0440 /etc/sudoers.d/$USERNAME

RUN apt-get upgrade -y \
    && apt-get update \
    && apt-get install -y python3-pip \
    && apt-get install -y vim \
    && apt install -y python-is-python3 \
    && apt-get install -y lsb-release wget gnupg \
    # ROS dependency
    && apt-get install -y ros-iron-rviz2 \
    # MavProxy and dependency
    && apt-get install -y libboost-all-dev \
    && apt-get install -y python3-dev python3-opencv python3-wxgtk4.0 python3-pip python3-matplotlib python3-lxml python3-pygame 

#Ardupilot
# Tools/environment_install/install-prereqs-ubuntu.sh -y needs to be run in the ardupilot directory
RUN git clone --recurse-submodules https://github.com/ArduPilot/ardupilot.git \
    # Gazebo Harmonic
    && wget https://packages.osrfoundation.org/gazebo.gpg -O /usr/share/keyrings/pkgs-osrf-archive-keyring.gpg \
    && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/pkgs-osrf-archive-keyring.gpg] http://packages.osrfoundation.org/gazebo/ubuntu-stable $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/gazebo-stable.list > /dev/null \
    && apt-get update -y \
    && apt-get install -y gz-harmonic \
    # ROS Gazebo Plugin
    && apt-get install -y ros-iron-ros-gzharmonic
    # Mavros - bridge between Ardupilot and ROS
RUN apt install -y ros-iron-mavros \
    && wget https://raw.githubusercontent.com/mavlink/mavros/ros2/mavros/scripts/install_geographiclib_datasets.sh \
    && chmod +x install_geographiclib_datasets.sh \
    && sudo ./install_geographiclib_datasets.sh

RUN pip3 install pexpect future \
    && pip3 install PyYAML mavproxy --user

# ArduPilot plugin for Gazebo
RUN apt install -y libgz-sim8-dev rapidjson-dev \
    && git clone https://github.com/ArduPilot/ardupilot_gazebo  \
    && mkdir -p ardupilot_gazebo/build \
    && cd ardupilot_gazebo/build \
    && cmake .. -DCMAKE_BUILD_TYPE=RelWithDebInfo \
    && make -j4

ENV SHELL /bin/bash
USER $USERNAME
CMD ["/bin/bash"]


# Gazebo Wave Simulator

#RUN apt-get update \
#   && apt-get install -y libgz-msgs9-dev \
#   && apt-get install -y libgz-transport12-dev  \
#  && apt-get install -y libgz-rendering7-dev \
#  && apt-get install -y libgz-sim7-dev \
#  && apt-get install -y libsdformat13-dev 

#RUN apt-get install -y libcgal-dev libfftw3-dev \
#    && mkdir gz_wavesim \
#    && cd gz_wavesim \
#    && mkdir src && cd src \
#    && git clone https://github.com/srmainwaring/asv_wave_sim.git \
#    && colcon build --merge-install --cmake-args \
#    -DCMAKE_BUILD_TYPE=RelWithDebInfo \
#    -DCMAKE_MACOSX_RPATH=FALSE \
#    -DCMAKE_INSTALL_NAME_DIR=$HOME/gz_wavesim/src/install/lib \
#    --packages-select gz-waves1

#RUN cd $HOME/gz_wavesim/src/asv_wave_sim/gz-waves/src/gui/plugins/waves_control \
#    && mkdir build && cd build \
#    && cmake .. && make

# Paths for Gazebo, Ardupilot_Gazebo new and corrected
#ENV GZ_SIM_RESOURCE_PATH="$HOME/gz_wavesim/src/asv_wave_sim/gz-waves-models/models:$HOME/gz_wavesim/src/asv_wave_sim/gz-waves-models/world_models:$HOME/gz_wavesim/src/asv_wave_sim/gz-waves-models/worlds"
#ENV GZ_SIM_SYSTEM_PLUGIN_PATH="$HOME/ardupilot_gazebo/build:$HOME/gz_wavesim/src/install/lib:$GZ_SIM_SYSTEM_PLUGIN_PATH"
#ENV GZ_SIM_RESOURCE_PATH="$HOME/ws/src/simulation/simulation/models:$HOME/ws/src/simulation/simulation/worlds:$HOME/ardupilot_gazebo/models:$HOME/ardupilot_gazebo/worlds:$GZ_SIM_RESOURCE_PATH"
#ENV LD_LIBRARY_PATH="$HOME/gz_wavesim/src/install/lib"
#ENV GZ_IP=127.0.0.1
#ENV GZ_CONFIG_PATH="/usr/share/gz:$HOME/gz_wavesim/install/share/gz"
#ENV GZ_GUI_PLUGIN_PATH="$HOME/gz_wavesim/src/asv_wave_sim/gz-waves/src/gui/plugins/waves_control/build"
