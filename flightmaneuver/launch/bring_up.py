from launch import LaunchDescription
from launch_ros.actions import Node

def generate_launch_description():
    return LaunchDescription([
        Node(
            package='flightmaneuver',
            executable='boat_control',
            name='boat_control',
            output='screen'
        ),
        Node(
            package='flightmaneuver',
            executable='change_flightmode',
            name='change_flightmode',
            output='screen'
        ),
        Node(
            package='flightmaneuver',
            executable='reposition',
            name='reposition',
            output='screen'
        ),
        Node(
            package='flightmaneuver',
            executable='planner',
            name='planner',
            output='screen'
        ),
        
        Node(
            package='flightmaneuver',
            executable='boat_position',
            name='boat_position',
            output='screen'
        ),

        Node(
            package='flightmaneuver',
            executable='boat_position_saver',
            name='boat_position_saver',
            output='screen'
        ),
    ])