#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from rclpy.action import ActionClient
from rclpy.callback_groups import ReentrantCallbackGroup
from sw_msgs.srv import TrackId
from sw_msgs.srv import Flightmode
from sw_msgs.action import Reposition

class PlannerNode(Node):

    def __init__(self):
        super().__init__('planner')
        self.callback_group = ReentrantCallbackGroup()
        # service taking maneuver initiation requests
        self.track_service = self.create_service(TrackId, 'track_service', self.track_request, callback_group=self.callback_group)
        # service taking maneuver initiation and cancel requests
        self.flightmode_service = self.create_service(Flightmode, 'flightmode_service', self.flightmode_callback, callback_group=self.callback_group)
        # client to flight_mode service
        self.change_flightmode_client = self.create_client(Flightmode, 'flightmode_service', callback_group=self.callback_group)
        while not self.change_flightmode_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Waiting for service change_flightmode_client...')
        # client to RepositionNode 
        self.reposition_client = ActionClient(self, Reposition, 'reposition_action_server', callback_group=self.callback_group)
        while not self.reposition_client.wait_for_server(timeout_sec=1.0):
            self.get_logger().info('Waiting for action server reposition...')
        # client to ChangeFlightmodeNode
        self.flightmode_client = self.create_client(Flightmode, 'setFlightmode', callback_group=self.callback_group)
        while not self.flightmode_client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Waiting for service setFlightmode...')
        self.goal_handle =  None
        self.current_track_id = None

    async def track_request(self, request, response):
        flightmode_request = Flightmode.Request()
        flightmode_request.mode = 'BEGIN_MANEUVER'
        future = self.flightmode_client.call_async(flightmode_request)
        try:
            flightmode_result = await future
            if flightmode_result.ack:
                reposition_future = self.request_action(request.track_id)
                reposition_result = await reposition_future
                if not reposition_result.accepted:
                    self.get_logger().info('Flightmaneuver request rejected')
                    response.ack = False
                else:
                    self.goal_handle = reposition_result
                    self.get_logger().info(f'Flightmaneuver request accepted: {self.goal_handle}')
                    response.ack = True
            else:
                response.ack = False
        except Exception as e:
                    self.get_logger().error(f'Service call failed with error: {e}')
                    response.ack = False
        return response

    async def flightmode_callback(self, request, response):
        if  self.goal_handle is not None and request.mode == "RETURN_TO_HOME" or "CONTINUE_MISSION":
            future = self.flightmode_client.call_async(request)
            try:
                result = await future
                if result.ack:
                    cancel_future = self.goal_handle.cancel_goal_async()
                    cancel_response = await cancel_future
                    if len(cancel_response.goals_canceling) > 0:
                        self.get_logger().info('Flightmaneuver successfully canceled')
                        response.ack = True
                    else:
                        self.get_logger().info('Flightmaneuver failed to cancel')
                        response.ack = False
            except Exception as e:
                self.get_logger().error(f'Service call failed with error: {e}')
                response.ack = False
        elif result.ack and request == "BEGIN_MANEUVER":
            response.ack = True
        else:
            self.get_logger().error(f'Service call failed with error: BAD DATA')
        return response

    def request_action(self, track_id):
        goal_msg = Reposition.Goal()
        goal_msg.track_id = track_id
        goal_future = self.reposition_client.send_goal_async(goal_msg, feedback_callback=self.feedback_callback)
        return goal_future

    def feedback_callback(self, feedback_msg):
        self.get_logger().info(f'Coordinates sent to autopilot: lat={feedback_msg.feedback.latitude},\
                               lon={feedback_msg.feedback.longitude}, alt={feedback_msg.feedback.altitude}')

    async def cancel_goal(self):
        cancel_future = self.goal_handle.cancel_goal_async()
        cancel_future.add_done_callback(self.cancel_callback, callback_group=self.callback_group)

    async def cancel_callback(self, future):
        cancel_response = future.result()
        if len(cancel_response.goals_canceling) > 0:
            self.get_logger().info('Successfully canceled goal')
        else:
            self.get_logger().info('Failed to cancel goal')

def main(args=None):
    rclpy.init(args=args)
    node = PlannerNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()