import rclpy
from rclpy.node import Node
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import Bool
import cv2
import numpy as np
import math
import os
import shutil


class RedObjectDetectorNode(Node):
    def __init__(self):
        super().__init__('red_object_detector')
        self.bridge = CvBridge()
        self.image_subscription = self.create_subscription(
            Image,
            '/sim/camera_right', 
            self.image_callback,
            10
        )
        self.image_subscription 
        self.publisher = self.create_publisher(Bool, '/detection', 10)
        self.distances = []
        self.count_detections = 0
        self.count_images = 0

    async def image_callback(self, msg):
        self.count_images += 1
        detected = Bool()
        detected.data = False
        try:
            image = self.bridge.imgmsg_to_cv2(msg, 'bgr8')
        except CvBridgeError as e:
            self.get_logger().error('No red object detected')
        # Get image dimensions and calculate the center
        height, width, _ = image.shape
        image_center = (width // 2, height // 2)

        # Convert the image to the HSV color space
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

        # Define the red color range in HSV
        lower_red1 = np.array([0, 50, 50])
        upper_red1 = np.array([10, 255, 255])
        lower_red2 = np.array([170, 50, 50])
        upper_red2 = np.array([180, 255, 255])

        # Create a mask for red color
        mask1 = cv2.inRange(hsv, lower_red1, upper_red1)
        mask2 = cv2.inRange(hsv, lower_red2, upper_red2)
        red_mask = cv2.add(mask1, mask2)
        
        kernel = np.ones((5, 5), np.uint8)
        red_mask = cv2.morphologyEx(red_mask, cv2.MORPH_CLOSE, kernel)

        # Find contours in the mask
        contours, _ = cv2.findContours(red_mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        if not contours:
            #self.get_logger().info('No contours found')
            return None

        distances = []

        for cnt in contours:
            if cv2.contourArea(cnt) > 300:  # Filters out small contours
                x, y, w, h = cv2.boundingRect(cnt)
                # Calculate the center of the bounding box
                object_center = (x + w // 2, y + h // 2)
                # Calculate distance from the center of the image
                distance = math.sqrt((object_center[0] - image_center[0]) ** 2 + (object_center[1] - image_center[1]) ** 2)
                distances.append(distance)
                detected = Bool()
                detected.data = True
                self.publisher.publish(detected)
                self.count_detections +=1
                #self.get_logger().info(f'Boats detected: {self.count_detections} ')
        # Return the smallest distance found
        return min(distances) if distances else None


def main(args=None):
    directory_path = '/home/searchwing/camera_right'

    if os.path.exists(directory_path):
        shutil.rmtree(directory_path)
    rclpy.init(args=args)
    red_object_detector = RedObjectDetectorNode()
    try:
        rclpy.spin(red_object_detector)
    except KeyboardInterrupt:
        red_object_detector.get_logger().info(f'Boats detected: {(red_object_detector.count_detections * 100)/red_object_detector.count_images}')
        red_object_detector.get_logger().info('Node stopped cleanly')
    except Exception as e:
        red_object_detector.get_logger().error(f'Exception in node: {e}')
    finally:
        red_object_detector.destroy_node()
        rclpy.shutdown()


if __name__ == '__main__':
    main()