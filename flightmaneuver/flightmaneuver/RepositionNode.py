#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from rclpy.action.server import GoalResponse, CancelResponse
from sw_msgs.msg import TrackArray
from mavros_msgs.srv import CommandInt
from rclpy.action import ActionServer
from sw_msgs.action import Reposition
from rclpy.callback_groups import ReentrantCallbackGroup
from rclpy.executors import MultiThreadedExecutor
import time

class RepositionNode(Node):
    def __init__(self):
        super().__init__('reposition')
        self.get_logger().info('Reposition Node Initialized')
        self.callback_group = ReentrantCallbackGroup()

        # Create an ActionServer
        self._action_server = ActionServer(
            self,
            Reposition,
            'reposition_action_server',
            self.execute_callback,
            cancel_callback=self.cancel_callback,
            goal_callback = self.goal_callback,
            callback_group=self.callback_group)
        # Server only accepts on goal at a time
        self.current_goal_handle = None
        self.current_track_id = None
        self.newest_position = None
        self.last_call_time = time.time()

        # Create subscription to boat GPS position topic
        self.subscriber = self.create_subscription(TrackArray, '/sim/vehicle/tracks',
            self.change_position_callback, 10, callback_group=self.callback_group)

        # Create client for the service
        self.client = self.create_client(CommandInt, '/mavros/cmd/command_int', callback_group=self.callback_group)
        while not self.client.wait_for_service(timeout_sec=1.0):
            self.get_logger().error('Service /mavros/cmd/command_int not available, waiting again...')

    async def change_position_callback(self, track_array_msg):
        if self.current_goal_handle is not None:
            if self.current_goal_handle.is_cancel_requested:
                self.get_logger().info('Goal canceled')
                self.current_goal_handle.canceled()
                self.current_goal_handle = None
                return
        for track in track_array_msg.tracks:
            if track.id == self.current_track_id:
                self.newest_position = track.gps_position
                break

    def goal_callback(self, goal_request):
        self.current_track_id = goal_request.track_id
        return GoalResponse.ACCEPT

    def cancel_callback(self, goal_handle):
        self.get_logger().info('Received a request to cancel the goal')
        self.first_goal_received = False
        self.current_track_id = None
        return CancelResponse.ACCEPT

    async def execute_callback(self, goal_handle):
        self.current_goal_handle = goal_handle
        self.get_logger().info(f'New Goal Handle: {self.current_goal_handle}')
        try:
            while not self.current_goal_handle.is_cancel_requested:
                if self.newest_position is not None:
                    current_time = time.time()
                    if current_time - self.last_call_time > 1.0:
                        self.last_call_time = current_time
                        # Create a MAVLink request based on received message
                        request = CommandInt.Request()
                        request.broadcast = False
                        request.frame = 3
                        request.command = 192
                        request.current = 0
                        request.autocontinue = 0
                        request.param1 = 0.0
                        request.param2 = 0.0
                        request.param3 = 0.0
                        request.param4 = 0.0
                        request.x = int(self.newest_position.latitude * 1e7)
                        request.y = int(self.newest_position.longitude * 1e7)
                        request.z = 100.0
                        self.target_position = (request.x, request.y, request.z)

                        # Call the service asynchronously
                        future = self.client.call_async(request)
                        future.add_done_callback(self.service_callback)
            self.get_logger().info('Goal canceled')
            self.current_goal_handle.canceled()
        except Exception as e:
            self.get_logger().error(f'Error in execute_callback: {e}')
        finally:
            self.current_goal_handle = None
        return

    def service_callback(self, future):
        if self.current_goal_handle.is_cancel_requested:
            self.get_logger().info('Goal canceled')
            self.current_goal_handle.canceled()
            self.current_goal_handle = None
            return
        try:
            response = future.result()
            if response.success:
                # Publish feedback
                if self.target_position is not None:
                    feedback = Reposition.Feedback()
                    feedback.latitude = float(self.target_position[0])
                    feedback.longitude = float(self.target_position[1])
                    feedback.altitude = self.target_position[2]
                    self.current_goal_handle.publish_feedback(feedback)
        except Exception as e:
                    self.get_logger().error(f'Service call failed with error: {e}')

def main(args=None):
    rclpy.init(args=args)
    reposition_node = RepositionNode()
    executor = MultiThreadedExecutor()
    rclpy.spin(reposition_node, executor=executor)
    reposition_node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()
