#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from mavros_msgs.srv import SetMode
from sw_msgs.srv import Flightmode
from rclpy.callback_groups import ReentrantCallbackGroup

class ChangeFlightmodeNode(Node):
    def __init__(self):
        super().__init__('change_flightmode')
        callback_group = ReentrantCallbackGroup()
        self.srv = self.create_service(Flightmode, 'setFlightmode', self.set_flightmode_callback, callback_group=callback_group)
        self.get_logger().info('Change Flightmode Node Initialized')
        
        self.client = self.create_client(SetMode, '/mavros/set_mode', callback_group=callback_group)
        while not self.client.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('Waiting for service /mavros/set_mode...')
        self.request_custom_mode = None
        self.request_base_mode = None
        
    async def set_flightmode_callback(self, request, response):
        try:
            self.getArdupilotMode(request)
            self.mavros_request = SetMode.Request()
            self.mavros_request.base_mode = int(self.request_base_mode)
            self.mavros_request.custom_mode = str(self.request_custom_mode)
            future = self.client.call_async(self.mavros_request)
            result = await future
        except Exception as e:
            self.get_logger().error(f'Service call failed with error: {e}')
            response.ack = False
        else:
            response.ack = result.mode_sent
        return response

    def getArdupilotMode(self, request):
        if request.mode == 'BEGIN_MANEUVER':
            self.request_base_mode = 216
            self.request_custom_mode = 'GUIDED'
        elif request.mode == 'RETURN_TO_HOME':
            self.request_base_mode = 0
            self.request_custom_mode = 'RTL'
        elif request.mode == 'CONTINUE_MISSION':
            self.request_base_mode = 220
            self.request_custom_mode = 'AUTO'
        else:
            raise ValueError
                    
def main(args=None):
    rclpy.init(args=args)
    node = ChangeFlightmodeNode()
    rclpy.spin(node)
    node.destroy_node()
    rclpy.shutdown()

if __name__ == '__main__':
    main()

