#!/usr/bin/env python3

import random
import rclpy
from rclpy.node import Node
from gps_msgs.msg import GPSFix
from sensor_msgs.msg import NavSatFix
from sw_msgs.msg import Track
from sw_msgs.msg import TrackArray
#from std_msgs.msg import Bool


class BoatPosition(Node):

    def __init__(self):
            super().__init__('boat_position')
            self.get_logger().info('Boat Position Node Initialized')
            #Subscription to boat gps topic
            self.subscriber = self.create_subscription(GPSFix, '/sim/vehicle/gps_position', self.boat_position_callback, 10)
            #Publisher of boat gps positions with added noise
            self.publisher = self.create_publisher(TrackArray, '/sim/vehicle/tracks', 10)
    
    def boat_position_callback(self, msg):
        lat_noise = random.uniform(-0.000090, 0.000090)
        lon_noise = random.uniform(-0.000141, 0.000141)
        msg.latitude = msg.latitude + lat_noise
        msg.longitude = msg.longitude + lon_noise
        gps_position = NavSatFix()
        gps_position.latitude = msg.latitude
        gps_position.longitude = msg.longitude
        track_msg = Track()
        track_msg.state = 0
        track_msg.id = 1
        track_msg.gps_position = gps_position
        track_array = TrackArray()
        track_array.tracks = [track_msg]
        self.publisher.publish(track_array)
        
def main(args=None):
    rclpy.init(args=args)
    boat_position= BoatPosition()
    rclpy.spin(boat_position)
    boat_position.destroy_node()
    rclpy.shutdown()
        
if __name__ == '__main__':
    main()
        
        
        
            