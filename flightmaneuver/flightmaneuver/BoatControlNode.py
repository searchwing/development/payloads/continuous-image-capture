#!/usr/bin/env python3

import rclpy 
from rclpy.node import Node
from geometry_msgs.msg import Twist

class BoatControlNode(Node):
    def __init__(self):
        super().__init__('boat_control')
        self.get_logger().info('Boat Control Node Initialized')
        
        # Parameters with default values
        self.declare_parameter('linear_x', 1.0)
        self.declare_parameter('angular_x', 0.0)

        # Get parameter values from CLI
        self.linear_x = self.get_parameter('linear_x').value
        self.angular_x = self.get_parameter('angular_x').value
        
        self.get_logger().info(f'Linear X: {self.linear_x}, Angular Z: {self.angular_x}')
        
        self.publisher_ = self.create_publisher(Twist, '/sim/vehicle/cmd_vel', 10)
        timer_period = 5  # seconds
        self.timer = self.create_timer(timer_period, self.publishVelocityCMD)
        
    def publishVelocityCMD(self):
        twist = Twist()
        twist.linear.x = self.linear_x
        twist.linear.y = 0.0
        twist.linear.z = 0.0

        twist.angular.x = self.angular_x
        twist.angular.y = 0.0
        twist.angular.z = 0.0
        self.publisher_.publish(twist)

def main(args=None):
    rclpy.init(args=args)
    boat_control = BoatControlNode()
    rclpy.spin(boat_control)
    boat_control.destroy_node()
    rclpy.shutdown()