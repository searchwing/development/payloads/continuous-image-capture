from setuptools import find_packages, setup

package_name = 'flightmaneuver'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='searchwing',
    maintainer_email='searchwing@todo.todo',
    description='TODO: Package description',
    license='Apache-2.0',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'boat_control = flightmaneuver.BoatControlNode:main',
            'change_flightmode = flightmaneuver.ChangeFlightmodeNode:main',
            'reposition = flightmaneuver.RepositionNode:main',
            'planner = flightmaneuver.PlannerNode:main',
            'boat_position = flightmaneuver.BoatPosition:main',
            'boat_detector = flightmaneuver.boat_detector:main',
            'boat_position_saver = flightmaneuver.boat_position_saver:main'
        ],
    },
)
