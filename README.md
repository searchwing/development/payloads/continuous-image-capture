### Flightmaneuver to track a detected boat and get more information about the situation in form of continuous image capture.

This project aims develop a system to dynamically track and orbit detetcted boats at lower altitudes, capturing high-resolution images for detailed analysis.   The goal is to improve the ground resolution of images captured by the UAV, addressing the current limitations where images with a resolution of 20 to 40 cm per pixel are often insufficient for identifying small objects like refugee boats with precision. This includes distinguishing between the number of people on board, the presence of children, and the overall condition of the boat. In order to have the possibility to use the captured images as evidence, the project also aims to ensure that the UAV continuously maintains the object in its camera's field of view.

This project is done as part of a bachelor thesis.


